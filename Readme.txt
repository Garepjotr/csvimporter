Example:
First create a mapping file. This is a JSON File that contains the analysis of the CSV file: 
  dotnet run -- cm -i test.csv -o mapping-postgres.json -t MyTableName -db Postgres

Then create the CREATE TABLE script based on the mapping
  dotnet run -- cs -i mapping-postgres.json

Optionally test of your ODBC driver and connectionstring are correct
  dotnet run -- tc -cs Driver={PostgreSQL Unicode};Server=localhost;Database=postgres;UID=postgres;

And last, import your data into the table
  dotnet run -- im -i test.csv -m mapping-postgres.json -cs Driver={PostgreSQL Unicode};Server=localhost;Database=postgres;UID=postgres; -db Postgres

The tool will request the password for the user in the connectionstring unless there is a password in the connectionstring or the commandline options.

Command line options:

CSVImporter
  Import CSV files into a database

Usage:
  CSVImporter [options] [command]

Options:
  -db, --databaseType <MySql|Postgres|SqlServer>  Name of the database server (Postgres, SqlServer, MySql) [default: Postgres]
  --version                                       Show version information
  -?, -h, --help                                  Show help and usage information

Commands:
  cc, CreateConnection  Create a connection file to make future imports easier
  cm, CreateMapping     Create a mapping for a csv file (this analyses column names and types)
  CreateScript, cs      Create a script to create the table based on the mapping
  im, ImportData        Import the data from the provided csv file to the specified table
  tc, TestConnection    Test the connection to the database using the supplied connectionstring


CreateConnection
  Create a connection file to make future imports easier

Usage:
  CSVImporter [options] CreateConnection

Options:
  -d, --driver <driver>                           The OBDC driver to use [default: ./psqlodbcw.so]
  -s, --server <server>                           The address of the server [default: localhost]
  -db, --database <database>                      The name of the database [default: mydatabase]
  -u, --user <user>                               The username of the database [default: admin]
  -o, --output <output>                           The file to write the connectionstring to. Writes to terminal when not provided
  -db, --databaseType <MySql|Postgres|SqlServer>  Name of the database server (Postgres, SqlServer, MySql) [default: Postgres]
  -?, -h, --help                                  Show help and usage information


CreateMapping
  Create a mapping for a csv file (this analyses column names and types)

Usage:
  CSVImporter [options] CreateMapping

Options:
  -i, --input <input> (REQUIRED)                  The path to the CSV file you want to analyse [default: import.csv]
  -t, --tableName <tableName>                     The name of the table [default: mytable]
  -d, --delim <delim>                             The delimiter used in the csv file [default: ;]
  -p, --pad <pad>                                 The percentage of padding (characters) to add to the longest string length of a column, also padds precision and scale of decimals [default: 50]
  -dc, --decimalChar <decimalChar>                The delimiter used in to denote the fraction in a decimal [default: .]
  -o, --output <output>                           The filename of the file to write the mapping to [default: mapping.json]
  -db, --databaseType <MySql|Postgres|SqlServer>  Name of the database server (Postgres, SqlServer, MySql) [default: Postgres]
  -?, -h, --help                                  Show help and usage information


CreateScript
  Create a script to create the table based on the mapping

Usage:
  CSVImporter [options] CreateScript

Options:
  -i, --input <input>                             The path to the mapping JSON file you want to base the script on [default: mapping.json]
  -o, --output <output>                           The filename of the file to write the script to. Writes to terminal when not provided
  -db, --databaseType <MySql|Postgres|SqlServer>  Name of the database server (Postgres, SqlServer, MySql) [default: Postgres]
  -?, -h, --help                                  Show help and usage information


ImportData
  Import the data from the provided csv file to the specified table

Usage:
  CSVImporter [options] ImportData

Options:
  -i, --csv <csv> (REQUIRED)                      The path to the CSV file you want to import
  -m, --mapping <mapping>                         The path to the mapping JSON file you want to base the import on. If not provided the CSV will be analysed before import with all default options and uses the
                                                  CSV filename as tablename.
  -cf, --conFile <conFile>                        The path to the connection file you want to base the import on
  -cs, --conString <conString>                    The connection string if no connection file was provided
  -p, --password <password>                       The password to use to connect to the database. If not provided it will be asked during import. Note, if a password is enetered this way it may be remembered in
                                                  your terminal history.
  -db, --databaseType <MySql|Postgres|SqlServer>  Name of the database server (Postgres, SqlServer, MySql) [default: Postgres]
  -?, -h, --help                                  Show help and usage information


TestConnection
  Test the connection to the database using the supplied connectionstring

Usage:
  CSVImporter [options] TestConnection

Options:
  -cf, --conFile <conFile>                        The path to the connection file you want to base the import on
  -cs, --conString <conString>                    The connection string if no connection file was provided
  -p, --password <password>                       The password to use to connect to the database. If not provided it will be asked during import. Note, if a password is enetered this way it may be remembered in
                                                  your terminal history.
  -db, --databaseType <MySql|Postgres|SqlServer>  Name of the database server (Postgres, SqlServer, MySql) [default: Postgres]
  -?, -h, --help                                  Show help and usage information
