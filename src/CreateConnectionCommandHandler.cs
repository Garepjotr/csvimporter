#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.IO;

namespace CSVImporter
{
    public class CreateConnectionCommandHandler
    {
        public void CreateConnection(string driver, string server, string database, string user, FileInfo output)
        {
            if(output != null && output.IsReadOnly)
            {
                Console.WriteLine("Output file is readonly");
                return;
            }
            try
            {
                var connectionString = $"Driver={driver};Server={server};Database={database};UID={user};";
                if (output != null)
                {
                    if (!output.Exists)
                        output.Create();

                    var writer = output.AppendText();
                    writer.WriteLine(connectionString);
                    writer.Close();
                }
                else
                {
                    Console.WriteLine(connectionString);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}