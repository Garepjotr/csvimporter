#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.IO;
using System.Text;
using CSVImporter.DatabaseHelpers;
using Newtonsoft.Json;

namespace CSVImporter
{
    public class CreateMappingCommandHandler
    {
        public void CreateMapping(FileInfo input, string tableName, char delim, int pad, char decimalChar, FileInfo output, DatabaseTypes databaseType)
        {
            if (!input.Exists)
            {
                Console.WriteLine("Input file does not exist");
                return;
            }
            if (delim == default(char))
            {
                Console.WriteLine("No delimiter set");
                return;
            }
            if (pad < 0)
            {
                Console.WriteLine("Padding cannot be negative");
                return;
            }
            if (output.Exists && output.Length > 0)
            {
                Console.WriteLine("Output file not empty");
                return;
            }

            Console.WriteLine($"Creating mapping for: {input}, with delim {delim}, with padding: {pad}, and outputting to {output}");
            
            var dbHelperFactory = new DatabaseHelperFactory();
            var helper = dbHelperFactory.GetHelper(databaseType);

            var reader = input.OpenText();
            var mapping = MappingCreator.CreateMapping(reader, tableName, delim, pad, decimalChar, helper);
            reader.Close();

            var writer = output.OpenWrite();
            var json = JsonConvert.SerializeObject(mapping, Formatting.Indented);
            var bytes = Encoding.UTF8.GetBytes(json);
            writer.Write(bytes, 0, bytes.Length);
            writer.Close();
        }
    }
}
