#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.IO;

namespace CSVImporter
{
    public class CreateScriptCommandHandler
    {
        public void CreateScript(FileInfo input, FileInfo output)
        {
            if(input == null || !input.Exists)
            {
                Console.WriteLine("Input not provided or accessible");
                return;
            }
            if(output.IsReadOnly)
            {
                Console.WriteLine("Output file is readonly");
                return;
            }
            try
            {
                var model = MappingCreator.CreateModelFromFile(input);
                var script = ScriptCreator.CreateScript(model);

                if (output != null)
                {
                    if (!output.Exists)
                        output.Create();

                    var writer = output.AppendText();
                    writer.WriteLine(script);
                    writer.Close();
                }
                else
                {
                    Console.WriteLine(script);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            } 
        }
    }
}
