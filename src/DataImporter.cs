#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.Data.Odbc;
using System.Globalization;
using System.IO;
using System.Text;
using CSVImporter.DatabaseHelpers;

namespace CSVImporter
{
    public class DataImporter
    {
        public static int ImportData(StreamReader csv, MappingModel model, string connectionString, IDatabaseHelper dbHelper)
        {
            using var db = new OdbcConnection(connectionString);
            db.Open();

            var line = csv.ReadLine(); //Skip columns
            var fieldCount = model.Columns.Count;

            var sb = new StringBuilder();
            var placeholderName = $"?";
            var query = $"INSERT INTO {model.Table} VALUES (";
            for (int i = 0; i < fieldCount; ++i)
            {
                query += placeholderName;
                if (i != fieldCount - 1)
                    query += ",";
            }
            query += $");";

            var insertCount = 0;
            while (!csv.EndOfStream)
            {
                line = csv.ReadLine();
                var parts = line.Split(model.Delimiter);

                using (var command = db.CreateCommand())
                {
                    for (int i = 0; i < fieldCount; ++i)
                    {
                        if (i > parts.Length)
                            continue;

                        var column = model.Columns[i];
                        var field = parts[i];

                        if (!string.IsNullOrEmpty(field))
                        {
                            if (column.FromDataType == "Bool" &&
                                bool.TryParse(field, out var boolValue))
                                command.Parameters.AddWithValue(placeholderName, boolValue);
                            else if (column.FromDataType == "DateTime" &&
                                DateTime.TryParseExact(field, model.DateTimeFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTimeValue))
                                command.Parameters.AddWithValue(placeholderName, dateTimeValue);
                            else if (column.FromDataType == "Guid" && Guid.TryParse(field, out var guidValue))
                                command.Parameters.AddWithValue(placeholderName, field);
                            else if (column.FromDataType == "Decimal" && Decimal.TryParse(field, out var decimalValue))
                                command.Parameters.AddWithValue(placeholderName, decimalValue);
                            else if (column.FromDataType == "Int" && Int32.TryParse(field, out var intValue))
                                command.Parameters.AddWithValue(placeholderName, intValue);
                            else if (column.FromDataType == "String")
                                command.Parameters.AddWithValue(placeholderName, field);
                        }
                        else
                        {
                            command.Parameters.AddWithValue(placeholderName, DBNull.Value);
                        }
                    }
                    command.CommandText = query;
                    insertCount += command.ExecuteNonQuery();
                }
            }

            return insertCount;
        }
    }
}
