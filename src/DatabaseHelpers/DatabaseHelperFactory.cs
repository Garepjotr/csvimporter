#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace CSVImporter.DatabaseHelpers
{
    public enum DatabaseTypes
    {
        Postgres,
        SqlServer,
        MySql
    }
    public class DatabaseHelperFactory
    {
        private List<Type> helperTypes;

        public IDatabaseHelper GetHelper(DatabaseTypes type)
        {
            if (helperTypes == null || !helperTypes.Any())
                helperTypes = GetHelperTypes();

            var typeName = Enum.GetName<DatabaseTypes>(type);
            var chosenType = helperTypes.Where(x => x.Name.StartsWith(typeName)).FirstOrDefault();
            if (chosenType == null)
                throw new TypeLoadException($"No implementation found for database type {typeName}");

            var helper = (IDatabaseHelper)Activator.CreateInstance(chosenType);
            return helper;
        }

        private List<Type> GetHelperTypes()
        {
            var type = typeof(IDatabaseHelper);
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetLoadableTypes())
                .Where(y => type.IsAssignableFrom(y) && !y.IsInterface)
                .ToList();
        }
    }
}
