#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

namespace CSVImporter.DatabaseHelpers
{
    public class MySqlHelper : IDatabaseHelper
    {
        public string WrapName(string name)
        {
            return $"`{name}`";
        }

        public string GetDatabaseType(string type)
        {
            switch(type)
            {
                case "Bool":
                    return "boolean";
                case "String":
                    return "varchar";
                case "DateTime":
                    return "datetime";
                case "Int":
                    return "int";
                case "Decimal":
                    return "decimal";
                case "Guid":
                    return "varchar(64)";
                default:
                    return "";
            }
        }
    }
}