#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System.IO;
using System;
using CSVImporter.DatabaseHelpers;

namespace CSVImporter
{
    public class ImportDataHandler
    {
        public async void ImportData(FileInfo csv, FileInfo mapping, FileInfo conFile, string conString, string password, DatabaseTypes databaseType)
        {
            var dbHelperFactory = new DatabaseHelperFactory();
            var helper = dbHelperFactory.GetHelper(databaseType);

            MappingModel model;
            if (mapping != null && mapping.Exists)
                model = MappingCreator.CreateModelFromFile(mapping);
            else
                model = MappingCreator.CreateMapping(csv.OpenText(), csv.Name.Substring(0, csv.Name.LastIndexOf('.')), ';', 50, '.', helper);

            var connectionString = conString;
            if (conFile != null && conFile.Exists)
                connectionString = await conFile.OpenText().ReadToEndAsync();
                
            if (string.IsNullOrEmpty(password) && !connectionString.Contains("PWD="))
            {
                Console.WriteLine("Please enter database password");
                password = Console.ReadLine();
            }
            if (!connectionString.EndsWith(";") && !connectionString.EndsWith("; "))
                connectionString += ";";
            connectionString += $"PWD={password}";

            var stream = csv.OpenText();
            var insertCount = DataImporter.ImportData(stream, model, connectionString, helper);

            Console.WriteLine($"Wrote {insertCount} lines to database");
        }
    }
}
