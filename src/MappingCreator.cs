#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using CSVImporter.DatabaseHelpers;
using Newtonsoft.Json;

namespace CSVImporter
{
    public class MappingCreator
    {
        public static MappingModel CreateModelFromFile(FileInfo input)
        {
            var reader = input.OpenText();
            var json = reader.ReadToEnd();
            reader.Close();
            var model = JsonConvert.DeserializeObject<MappingModel>(json, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.None });
            return model;
        }

        public static MappingModel CreateMapping(StreamReader input, string tableName, char delim, int pad, char decimalChar, IDatabaseHelper dbHelper)
        {
            var dateTimeFormats = "yyyy-MM-dd;yyyy-MM-dd hh:mm:ss;".Split(';');
            var mappingModel = new MappingModel();
            mappingModel.DecimalChar = decimalChar;
            mappingModel.Delimiter = delim;
            mappingModel.Table = dbHelper.WrapName(tableName);
            mappingModel.DateTimeFormats = dateTimeFormats.ToList();
            var readColumns = true;

            while (!input.EndOfStream)
            {
                var line = input.ReadLine();
                var parts = line.Split(delim);
                if (readColumns)
                {
                    readColumns = false;
                    mappingModel.Columns = new List<Column>(parts.Length);
                    foreach (var columnName in parts)
                    {
                        mappingModel.Columns.Add(new Column() { Name = dbHelper.WrapName(columnName) });
                    }
                }
                else
                {
                    var fieldCount = Math.Min(parts.Length, mappingModel.Columns.Count);
                    for (int i = 0; i < fieldCount; ++i)
                    {
                        var field = parts[i];
                        if (string.IsNullOrEmpty(field))
                            continue;

                        var column = mappingModel.Columns[i];
                        if (column.FromDataType != "DateTime" &&
                            column.FromDataType != "Guid" &&
                            column.FromDataType != "Decimal" &&
                            column.FromDataType != "Int" &&
                            column.FromDataType != "String" &&
                            bool.TryParse(field, out _))
                        {
                            column.FromDataType = "Bool";
                        }
                        else if (column.FromDataType != "Guid" &&
                            column.FromDataType != "Decimal" &&
                            column.FromDataType != "Int" &&
                            column.FromDataType != "String" &&
                            DateTime.TryParseExact(field, dateTimeFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
                        {
                            column.FromDataType = "DateTime";
                        }
                        else if (column.FromDataType != "Decimal" &&
                            column.FromDataType != "Int" &&
                            column.FromDataType != "String" &&
                            Guid.TryParse(field, out _))
                        {
                            column.FromDataType = "Guid";
                        }
                        else if (column.FromDataType != "Decimal" &&
                            column.FromDataType != "String" &&
                            int.TryParse(field, out var intValue))
                        {
                            column.FromDataType = "Int";
                        }
                        else if (column.FromDataType != "String" &&
                            field.Contains(decimalChar) &&
                            decimal.TryParse(field, out var decimalValue))
                        {
                            column.FromDataType = "Decimal";
                            var sqlDecimal = new SqlDecimal(decimalValue);
                            column.Precision = Math.Max(column.Precision, sqlDecimal.Precision);
                            column.Scale = Math.Max(column.Scale, sqlDecimal.Scale);
                        }
                        else
                        {
                            column.FromDataType = "String";
                            column.Length = Math.Max(column.Length ?? 0, field.Length);
                        }
                    }
                }
            }
            var padding = ((float)pad / 100) + 1;
            foreach (var column in mappingModel.Columns)
            {
                column.ToDataType = dbHelper.GetDatabaseType(column.FromDataType);
                if (column.Length.HasValue)
                    column.Length = (int)((float)column.Length * padding);

                column.Precision += column.Scale;
                column.Precision = (int)((float)column.Precision * padding);
                column.Scale = (int)((float)column.Scale * padding);
                if (string.IsNullOrEmpty(column.ToDataType))
                    column.FromDataType = "String";
            }

            return mappingModel;
        }
    }
}
