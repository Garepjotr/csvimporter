#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System.Collections.Generic;

namespace CSVImporter
{
    public class MappingModel
    {
        public string Table { get; set; }
        public char Delimiter { get; set; }
        public char DecimalChar { get; set; }
        public List<string> DateTimeFormats { get; set; }
        public List<Column> Columns { get; set; }
    }

    public class Column
    {
        public string Name { get; set; }
        public string FromDataType { get; set; }
        public string ToDataType { get; set; }
        public int Precision { get; set; }
        public int Scale { get; set; }
        public int? Length { get; set; }

    }
}
