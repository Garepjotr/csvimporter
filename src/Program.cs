﻿#region License
// CSVImporter, imports CSV files into a database
// Copyright (C) 2021  Garepjotr

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using CSVImporter.DatabaseHelpers;

namespace CSVImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Here we go");

            var rCmd = new RootCommand("Import CSV files into a database");

            rCmd.AddCommand(CreateConnectionCommand());
            rCmd.AddCommand(CreateMappingCommand());
            rCmd.AddCommand(CreateScriptCommand());
            rCmd.AddCommand(ImportDataCommand());
            rCmd.AddCommand(TestConnectionCommand());

            var dbTypeOpt = new Option<DatabaseTypes>(
               "--databaseType",
               "Name of the database server (Postgres, SqlServer, MySql)");
            dbTypeOpt.AddAlias("-db");
            dbTypeOpt.SetDefaultValue("Postgres");
            rCmd.AddGlobalOption(dbTypeOpt);

            rCmd.Invoke(args);
        }

        private static Command TestConnectionCommand()
        {
            var testConnectionCommand = new Command(
                "TestConnection",
                "Test the connection to the database using the supplied connectionstring");
            testConnectionCommand.AddAlias("tc");

            var conOpt = new Option<FileInfo>(
                "--conFile",
                "The path to the connection file you want to base the import on");
            conOpt.AddAlias("-cf");
            conOpt.LegalFilePathsOnly();
            conOpt.ExistingOnly();
            testConnectionCommand.AddOption(conOpt);

            var constrOpt = new Option<string>(
                "--conString",
                "The connection string if no connection file was provided");
            constrOpt.AddAlias("-cs");
            testConnectionCommand.AddOption(constrOpt);

            var passOpt = new Option<string>(
                "--password",
                "The password to use to connect to the database. If not provided it will be asked during import. Note, if a password is entered this way it may be remembered in your terminal history.");
            passOpt.AddAlias("-p");
            testConnectionCommand.AddOption(passOpt);

            var handler = new TestConnectionHandler();
            testConnectionCommand.Handler =
                CommandHandler.Create<FileInfo, string, string>(handler.TestConnection);
            return testConnectionCommand;
        }

        private static Command ImportDataCommand()
        {
            var importDataCommand = new Command(
                "ImportData",
                "Import the data from the provided csv file to the specified table");
            importDataCommand.AddAlias("im");

            var csvOpt = new Option<FileInfo>(
                "--csv", "The path to the CSV file you want to import");
            csvOpt.AddAlias("-i");
            csvOpt.IsRequired = true;
            csvOpt.LegalFilePathsOnly();
            csvOpt.ExistingOnly();
            importDataCommand.AddOption(csvOpt);

            var mappingOpt = new Option<FileInfo>(
                "--mapping",
                "The path to the mapping JSON file you want to base the import on. If not provided the CSV will be analysed before import with all default options and uses the CSV filename as tablename.");
            mappingOpt.AddAlias("-m");
            mappingOpt.LegalFilePathsOnly();
            mappingOpt.ExistingOnly();
            importDataCommand.AddOption(mappingOpt);

            var conOpt = new Option<FileInfo>(
                "--conFile",
                "The path to the connection file you want to base the import on");
            conOpt.AddAlias("-cf");
            conOpt.LegalFilePathsOnly();
            conOpt.ExistingOnly();
            importDataCommand.AddOption(conOpt);

            var constrOpt = new Option<string>(
                "--conString",
                "The connection string if no connection file was provided");
            constrOpt.AddAlias("-cs");
            importDataCommand.AddOption(constrOpt);

            var passOpt = new Option<string>(
                "--password",
                "The password to use to connect to the database. If not provided it will be asked during import. Note, if a password is entered this way it may be remembered in your terminal history.");
            passOpt.AddAlias("-p");
            importDataCommand.AddOption(passOpt);

            var handler = new ImportDataHandler();
            importDataCommand.Handler =
                CommandHandler.Create<FileInfo, FileInfo, FileInfo, string, string,
                                      DatabaseTypes>(handler.ImportData);
            return importDataCommand;
        }

        private static Command CreateScriptCommand()
        {
            var createScriptCommand =
                new Command("CreateScript",
                            "Create a script to create the table based on the mapping");
            createScriptCommand.AddAlias("cs");

            var infileOpt = new Option<FileInfo>(
                "--input",
                "The path to the mapping JSON file you want to base the script on");
            infileOpt.AddAlias("-i");
            infileOpt.SetDefaultValue(new FileInfo("mapping.json"));
            infileOpt.LegalFilePathsOnly();
            infileOpt.ExistingOnly();
            createScriptCommand.AddOption(infileOpt);

            var outfileOpt = new Option<FileInfo>(
                "--output",
                "The filename of the file to write the script to. Writes to terminal when not provided");
            outfileOpt.AddAlias("-o");
            createScriptCommand.AddOption(outfileOpt);

            var handler = new CreateScriptCommandHandler();
            createScriptCommand.Handler =
                CommandHandler.Create<FileInfo, FileInfo>(handler.CreateScript);
            return createScriptCommand;
        }

        private static Command CreateMappingCommand()
        {
            var createMappingCmd = new Command(
                "CreateMapping",
                "Create a mapping for a csv file (this analyses column names and types)");
            createMappingCmd.AddAlias("cm");

            var infileOpt = new Option<FileInfo>(
                "--input", "The path to the CSV file you want to analyse");
            infileOpt.AddAlias("-i");
            infileOpt.SetDefaultValue(new FileInfo("import.csv"));
            infileOpt.LegalFilePathsOnly();
            infileOpt.ExistingOnly();
            infileOpt.IsRequired = true;
            createMappingCmd.AddOption(infileOpt);

            var tableOpt = new Option<string>("--tableName", "The name of the table");
            tableOpt.AddAlias("-t");
            tableOpt.SetDefaultValue("mytable");
            createMappingCmd.AddOption(tableOpt);

            var delimOpt =
                new Option<char>("--delim", "The delimiter used in the csv file");
            delimOpt.AddAlias("-d");
            delimOpt.SetDefaultValue(';');
            createMappingCmd.AddOption(delimOpt);

            var paddingOpt = new Option<int>(
                "--pad",
                "The percentage of padding (characters) to add to the longest string length of a column, also pads precision and scale of decimals");
            paddingOpt.AddAlias("-p");
            paddingOpt.SetDefaultValue(50);
            createMappingCmd.AddOption(paddingOpt);

            var decimalOpt = new Option<char>(
                "--decimalChar",
                "The delimiter used in to denote the fraction in a decimal");
            decimalOpt.AddAlias("-dc");
            decimalOpt.SetDefaultValue('.');
            createMappingCmd.AddOption(decimalOpt);

            var outfileOpt = new Option<FileInfo>(
                "--output", "The filename of the file to write the mapping to");
            outfileOpt.AddAlias("-o");
            outfileOpt.SetDefaultValue("mapping.json");
            createMappingCmd.AddOption(outfileOpt);

            var handler = new CreateMappingCommandHandler();
            createMappingCmd.Handler =
                CommandHandler
                    .Create<FileInfo, string, char, int, char, FileInfo, DatabaseTypes>(
                        handler.CreateMapping);
            return createMappingCmd;
        }

        private static Command CreateConnectionCommand()
        {
            var createConnectionCmd =
                new Command("CreateConnection",
                            "Create a connection file to make future imports easier");
            createConnectionCmd.AddAlias("cc");

            var driverOpt = new Option<string>("--driver", "The OBDC driver to use");
            driverOpt.AddAlias("-d");
            driverOpt.SetDefaultValue("./psqlodbcw.so");
            createConnectionCmd.AddOption(driverOpt);

            var serverOpt = new Option<string>("--server", "The address of the server");
            serverOpt.AddAlias("-s");
            serverOpt.SetDefaultValue("localhost");
            createConnectionCmd.AddOption(serverOpt);

            var dbOpt = new Option<string>("--database", "The name of the database");
            dbOpt.AddAlias("-db");
            dbOpt.SetDefaultValue("mydatabase");
            createConnectionCmd.AddOption(dbOpt);

            var userOpt = new Option<string>("--user", "The username of the database");
            userOpt.AddAlias("-u");
            userOpt.SetDefaultValue("admin");
            createConnectionCmd.AddOption(userOpt);

            var outputOpt = new Option<FileInfo>(
                "--output",
                "The file to write the connectionstring to. Writes to terminal when not provided");
            outputOpt.AddAlias("-o");
            createConnectionCmd.AddOption(outputOpt);

            var handler = new CreateConnectionCommandHandler();
            createConnectionCmd.Handler =
                CommandHandler.Create<string, string, string, string, FileInfo>(
                    handler.CreateConnection);
            return createConnectionCmd;
        }
    }
}
