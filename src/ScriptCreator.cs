#region License
    // CSVImporter, imports CSV files into a database
    // Copyright (C) 2021  Garepjotr

    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 2 of the License, or
    // (at your option) any later version.

    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.

    // You should have received a copy of the GNU General Public License along
    // with this program; if not, write to the Free Software Foundation, Inc.,
    // 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#endregion

namespace CSVImporter
{
    public class ScriptCreator
    {
        public static string CreateScript(MappingModel model)
        {
            var script = $"CREATE TABLE {model.Table}\r\n(\r\n";
            var count = model.Columns.Count;
            for (int i = 0; i < count; ++i)
            {
                var column = model.Columns[i];
                var dataType = column.ToDataType;
                switch (column.FromDataType)
                {
                    case "Decimal":
                        dataType += $"({column.Precision}, {column.Scale})";
                        break;
                    case "String":
                        dataType += $"({column.Length?.ToString() ?? "MAX"})";
                        break;
                    default:
                        break;
                }
                script += $"\t{column.Name}\t{dataType}\tnull";
                if(i == count -1)
                    script += "\r\n";
                else
                    script += ",\r\n";
            }
            script += ");";

            return script;
        }
    }
}
